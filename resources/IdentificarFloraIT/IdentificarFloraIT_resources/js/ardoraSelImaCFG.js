//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var timeAct=360; timeIni=360; timeBon=0;
var successes=0; successesMax=2; attempts=0; attemptsMax=2;
var score=0; scoreMax=2; scoreInc=1; scoreDec=1
var typeGame=0;
var tiTime=true;
var tiTimeType=2;
var tiButtonTime=true;
var textButtonTime="Comenzar";
var tiSuccesses=true;
var tiAttempts=false;
var tiScore=true;
var startTime;
var colorBack="#FFFDFD"; colorButton="#91962F"; colorText="#000000"; colorSele="#FF8000";
var goURLNext=false; goURLRepeat=false;tiAval=false;
var scoOk=0; scoWrong=0; scoOkDo=0; scoWrongDo=0; scoMessage=""; scoPtos=10;
var fMenssage="Tahoma, Geneva, sans-serif";
var fActi="Tahoma, Geneva, sans-serif";
var fPreg="Tahoma, Geneva, sans-serif";
var fEnun="Tahoma, Geneva, sans-serif";
var timeOnMessage=3; messageOk="¡Muy bien!"; messageTime="¡Se acabó el tiempo!";messageError="¡Volvé a intentarlo!";messageErrorG="¡Volvé a intentarlo!"; messageAttempts=""; isShowMessage=false;
var urlOk=""; urlTime=""; urlError=""; urlAttempts="";
var goURLOk="_blank"; goURLTime="_blank"; goURLAttempts="_blank"; goURLError="_blank"; 
borderOk="#008000"; borderTime="#FF0000";borderError="#FF0000"; borderAttempts="#FF0000";
var actMaxWidth="600"; actMaxHeight="";indexQ=0;dirMedia="IdentificarFloraIT_resources/media/";
var quest=[["","","Consigna_flores_ischigulasto.jpg"],["","","Consigna_flora_talampaya.jpg"]];
var altQuest=["",""];
var media=[["MQ==","QWxnYXJyb2JvIEJsYW5jbw==","0_algarrobo_blanco.jpg",""],["Mg==","QW1hcG9sYSBTaWx2ZXN0cmU=","0_Amapola_silvestre.jpg",""],["Mw==","QnJlYQ==","0_Brea.jpg",""],["NA==","Q2FjdHVz","0_Cactus.jpg",""],["NQ==","RmlxdWU=","0_fique.jpg",""],["Ng==","SGllcmJhIFBhbXBh","0_Hierba_de_pampa.jpg",""],["Nw==","TWFsdmE=","0_Malva.jpg",""],["OA==","T3JxdcOtZGVh","0_Orqu__dea.jpg",""],["OQ==","WmFtcGE=","0_Zampa.jpg",""],["MTA=","SmFycmlsbGE=","0_Jarrilla.jpg",""]];
var alt=["","","","","","","","","",""];
var dat=[["MQ==","MQ==","MA==","MQ==","MQ==","MA=="],["MQ==","Mw==","MA==","MQ==","MQ==","MA=="],["MQ==","NA==","MQ==","MQ==","MQ==","MA=="],["MQ==","NQ==","MQ==","MQ==","MQ==","MA=="],["MQ==","Ng==","MA==","MQ==","MQ==","MA=="],["MQ==","Nw==","MA==","MQ==","MQ==","MA=="],["MQ==","OQ==","MQ==","MQ==","MQ==","MA=="],["Mg==","MQ==","MQ==","MQ==","MQ==","MA=="],["Mg==","Mw==","MQ==","MQ==","MQ==","MA=="],["Mg==","NA==","MA==","MQ==","MQ==","MA=="],["Mg==","NQ==","MA==","MQ==","MQ==","MA=="],["Mg==","OQ==","MA==","MQ==","MQ==","MA=="],["Mg==","MTA=","MQ==","MQ==","MQ==","MA=="]];
var actualBoard=[];actualState=[];indexGame=1;tiAudio=false;
var wordsGame="SWRlbnRpZmljYXJGbG9yYUlU"; wordsStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function giveZindex(typeElement){var valueZindex=0; capas=document.getElementsByTagName(typeElement);
for (i=0;i<capas.length;i++){if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){valueZindex=parseInt($(capas[i]).css("z-index"),10);}}return valueZindex;}
var actorder=[1,2];var in_act=0;
