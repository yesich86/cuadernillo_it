//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var incre=0; timerMove=0;$(document).ready(function () {
$("#area_1").on("click touchstart", function (e) {imaUp("MapaIT_resources/media/1_El_hongo.jpg","");});
$("#area_1").keydown(function(e){if (e.which!=9){$("#area_1").click();}});
$('<div class="tooltip" id="tt_1">El Hongo<div class="arrow"></div></div>').appendTo("body");
$("#area_2").on("click touchstart", function (e) {imaUp("MapaIT_resources/media/2_El_Monje-Talampaya.jpg","");});
$("#area_2").keydown(function(e){if (e.which!=9){$("#area_2").click();}});
$('<div class="tooltip" id="tt_2">El Monje<div class="arrow"></div></div>').appendTo("body");
paintpoints();$( window ).resize(function() {paintpoints();});
jQuery.event.special.touchstart = {setup: function( _, ns, handle ) {this.addEventListener("touchstart", handle, { passive: !ns.includes("noPreventDefault")});}};
jQuery.event.special.touchmove = {setup: function( _, ns, handle ) {this.addEventListener("touchmove", handle, { passive: !ns.includes("noPreventDefault")});}};
});
function paintpoints(){var offset = $("#ima").offset();var offsetzoomer=$("#zoomer").offset();
$("#pt_1").remove();
var newpoint='<svg role="img" aria-label="point 1" id="pt_1" class="ctt_1" width="18" height="18" viewBox="0 0 5.292 5.292" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-93.226 -136.726)"><circle cx="95.872" cy="139.372" r="2.185" opacity=".923" fill="#FFB871" fill-rule="evenodd" stroke="#FFB871" stroke-width=".237"/><circle cx="95.872" cy="139.372" r="2.51" opacity=".923" fill="none" stroke="#91962F" stroke-width=".272"/></g></svg>';
$("#zoomer").append(newpoint);
$("#pt_1").css("position", "fixed");$("#pt_1").css("top",(offset.top+305).toString()+"px");
$("#pt_1").css("left",(offset.left+231).toString()+ "px");$("#pt_1").css("cursor","pointer");$("#pt_1").attr("alt","");
if (offset.left+231<offsetzoomer.left || offset.left+231>offsetzoomer.left+497 || offset.top+305<offsetzoomer.top || offset.top+305>offsetzoomer.top+467){$("#pt_1").remove();}
$("#pt_1").attr("tabindex","0");
$("#pt_1").on("click touchstart", function(e) {$("#area_1").click();});
$(".ctt_1").hover(function() {var linkPosition = $("#pt_1").position();$tooltip=$("#tt_1");$tooltip.css({top: linkPosition.top-$tooltip.outerHeight()-13,left: linkPosition.left - ($tooltip.width()/2)});$tooltip.addClass("active");
}, function() {$tooltip = $("#tt_1").addClass("out");setTimeout(function() {$tooltip.removeClass("active").removeClass("out");}, 300);});
$("#area_1").focus(function(e){$(".tooltip").removeClass("active").removeClass("out");var linkPosition = $("#pt_1").position();$tooltip=$("#tt_1");$tooltip.css({top: linkPosition.top -$tooltip.outerHeight()-13,left: linkPosition.left - ($tooltip.width()/2)});$tooltip.addClass("active");});
$("#area_1").blur(function(e){$tooltip = $("#tt_1").addClass("out");});
$("#pt_1").keydown(function(e){if (e.which!=9){$("#area_1").click();}});
$("#pt_2").remove();
var newpoint='<svg role="img" aria-label="point 2" id="pt_2" class="ctt_2" width="18" height="18" viewBox="0 0 5.292 5.292" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-93.226 -136.726)"><circle cx="95.872" cy="139.372" r="2.185" opacity=".923" fill="#FFB871" fill-rule="evenodd" stroke="#FFB871" stroke-width=".237"/><circle cx="95.872" cy="139.372" r="2.51" opacity=".923" fill="none" stroke="#91962F" stroke-width=".272"/></g></svg>';
$("#zoomer").append(newpoint);
$("#pt_2").css("position", "fixed");$("#pt_2").css("top",(offset.top+115).toString()+"px");
$("#pt_2").css("left",(offset.left+305).toString()+ "px");$("#pt_2").css("cursor","pointer");$("#pt_2").attr("alt","");
if (offset.left+305<offsetzoomer.left || offset.left+305>offsetzoomer.left+497 || offset.top+115<offsetzoomer.top || offset.top+115>offsetzoomer.top+467){$("#pt_2").remove();}
$("#pt_2").attr("tabindex","0");
$("#pt_2").on("click touchstart", function(e) {$("#area_2").click();});
$(".ctt_2").hover(function() {var linkPosition = $("#pt_2").position();$tooltip=$("#tt_2");$tooltip.css({top: linkPosition.top-$tooltip.outerHeight()-13,left: linkPosition.left - ($tooltip.width()/2)});$tooltip.addClass("active");
}, function() {$tooltip = $("#tt_2").addClass("out");setTimeout(function() {$tooltip.removeClass("active").removeClass("out");}, 300);});
$("#area_2").focus(function(e){$(".tooltip").removeClass("active").removeClass("out");var linkPosition = $("#pt_2").position();$tooltip=$("#tt_2");$tooltip.css({top: linkPosition.top -$tooltip.outerHeight()-13,left: linkPosition.left - ($tooltip.width()/2)});$tooltip.addClass("active");});
$("#area_2").blur(function(e){$tooltip = $("#tt_2").addClass("out");});
$("#pt_2").keydown(function(e){if (e.which!=9){$("#area_2").click();}});
}
function imaUp(ima,alt){$("body").css("overflow","hidden");$("#ardoraActCanvas").attr({"width": $(window).width(),"height": $(window).height()});document.getElementById("ardoraActCanvas").style.zIndex=5;document.getElementById("ardoraActCanvas").style["visibility"]="visible";
var alpha=0;var delta=0.03;var canvas=document.getElementById("ardoraActCanvas");var context=canvas.getContext("2d");context.beginPath();context.fillStyle="rgba(0, 0, 0, 0.25)";context.fillRect(0, 0, context.canvas.width, context.canvas.height);context.fill();var imageObj=new Image();
imageObj.onload = function() {var xCenterIma=($("#ardoraActCanvas").width()-imageObj.width) / 2;var yCenterIma=($("#ardoraActCanvas").height()-imageObj.height) / 2;context.beginPath();context.shadowColor ="rgba(0, 0, 0, 0.25)";context.shadowBlur = 10;
context.shadowOffsetX=10;context.shadowOffsetY=10;context.fill();context.beginPath();context.strokeStyle="rgba(0,0,0,1)";context.fillStyle ="rgba(255,255,255,1)";context.fillRect(xCenterIma-5,yCenterIma-5,imageObj.width+10,imageObj.height+10);
context.fill();context.rect(xCenterIma-5,yCenterIma-5,imageObj.width+10,imageObj.height+10);context.stroke();context.fill();context.shadowBlur=0;context.shadowOffsetX=0;context.shadowOffsetY = 0;loop();
function loop(){alpha+=delta;if (alpha<=1){context.clearRect(xCenterIma,yCenterIma,imageObj.width,imageObj.height);context.globalAlpha=alpha;context.drawImage(imageObj,xCenterIma,yCenterIma);requestAnimationFrame(loop);}}};
imageObj.src =ima;$("#ardoraActCanvas").css("cursor", "pointer");$("#ardoraActCanvas").click(function() {document.getElementById("ardoraActCanvas").style.zIndex=0;document.getElementById("ardoraActCanvas").style["visibility"]="hidden";canvas.width=canvas.width;$("body").css("overflow","visible");$("#ardoraActCanvas").attr({"width":2,"height":2});});
$("#ardoraActCanvas").attr("tabindex","0");$("#ardoraActCanvas").focus();$("#ardoraActCanvas").attr("role","img");$("#ardoraActCanvas").attr("aria-label",alt);
$("#ardoraActCanvas").keydown(function(e){if (e.which==27){$("#ardoraActCanvas").click();}});
}
function playIt(sound){$("audio").each(function(){this.pause();}); var file = document.getElementById(sound);file.load();file.play();}
window.requestAnimationFrame=(function(){return  window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame    || function( callback ){window.setTimeout(callback, 10000 / 60);};})();
